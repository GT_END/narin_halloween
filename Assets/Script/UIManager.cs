using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : Singleton<UIManager>
{
    [SerializeField] Text TimeUI;
    [SerializeField] Text Score;
    [SerializeField] RectTransform gameoverPanel;
    [SerializeField] Text scoreResault;
    [SerializeField] RectTransform startgamePanel;

    float currentTime;
    int currentScore;
 

    public void UpdateTimeUI()
    {
        currentTime += Time.deltaTime;
        SetTimeText($"Time : {string.Format("{0:0.00}", currentTime)}");
    }
    public void UpdateScore(int Score)
    {
        SetScoreText($"Score : {Score}");
        SetScoreResault($"Score : {Score}");
    }

    public void SetTimeText(string text)
    {

        TimeUI.text = text;
    }
    public void SetScoreText(string text)
    {
        Score.text = text;
    }
    public void SetScoreResault(string text)
    {
        scoreResault.text = text;
    }

    public void ResetUI()
    {
        
        currentTime = 0;
        currentScore = 0;
        UpdateTimeUI();
        UpdateScore(0);
        
    }

    public void SetGameoverActive(bool isActive)
    {
        gameoverPanel.gameObject.SetActive(isActive);
    }
    public void SetStartGameActive(bool isActive)
    {
        startgamePanel.gameObject.SetActive(isActive);
    }



}
