﻿using UnityEngine;

public class Singleton<T> : MonoBehaviour
{
    private static T _instance;
    public static T Instance { get => _instance; set => _instance = value; }

    protected virtual void Awake()
    {
        
            _instance = (T)(object)this;
        

        //DontDestroyOnLoad(this.gameObject);
    }

    protected virtual void Start()
    {

    }
}
