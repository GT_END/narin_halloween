using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class GhostNavUnit : MonoBehaviour
{
    [SerializeField] NavMeshAgent agent;
    [SerializeField] Transform follow;
    [SerializeField] SpriteRenderer spriteRanderer;

    private void Start()
    {
        SetPosition();  
    }

    public void SetLifeTime(float lifetime)
    {

        Invoke("DestoryItem", lifetime);
    }
    public void DestoryItem()
    {
        Destroy(this.gameObject);
    }

    public void SetPosition()
    {
        var pos = GameManager.Instance.GetRandomPosition();
        agent.SetDestination(pos);

        var rndtime = Random.Range(GameManager.Instance.setting.GhostSettings.minSpawnTime, GameManager.Instance.setting.GhostSettings.maxSpawnTime);

        var horizon = transform.position.x - pos.x;

        if (horizon > 0)
        {
            spriteRanderer.flipX = false;
        }
        else if (horizon < 0)
        {
            spriteRanderer.flipX = true;
        }

        Invoke("SetPosition",rndtime);
    }

}
