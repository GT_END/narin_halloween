using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameSetting", menuName = "ScriptableObjects/GameSetting")]
[System.Serializable]
public class GameSetting : ScriptableObject
{
    public ItemSetting ItemSettings;
    public GhostSetting GhostSettings;

}

[System.Serializable]
public class ItemSetting
{
    public float minSpawnTime;
    public float maxSpawnTime;
    public float itemLifeTime;

}
[System.Serializable]
public class GhostSetting
{
    public float minSpawnTime;
    public float maxSpawnTime;
    public float LifeTime;
}
