using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovement : MonoBehaviour
{
    [SerializeField] float movespeed;
    [SerializeField] Animator anim;
    [SerializeField] SpriteRenderer spriteRanderer;


    Vector3 playerposition;
    Rigidbody rigid;

    // Start is called before the first frame update
    void Start()
    {
        if (rigid == null)
            rigid = GetComponent<Rigidbody>();
        if(spriteRanderer == null)
        {
            spriteRanderer = GetComponentInChildren<SpriteRenderer>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!GameManager.Instance.isGameover)
        {
            UpdatePlayerPosition();
            UpdateAnimation();
        }
    }

    void UpdateAnimation()
    {
        if(Input.anyKey)
        {
            anim.SetBool("isWalk", true);
        }
        else
            anim.SetBool("isWalk", false);
    }

    private void MoveHorizontal()
    {
        float horizon = Input.GetAxis("Horizontal");

        //playerposition.x += horizon*movespeed;
        rigid.AddForce(new Vector3((horizon * movespeed), 0,0));

        if(horizon >0)
        {
            spriteRanderer.flipX = true;
        }
        else if (horizon < 0)
        {
            spriteRanderer.flipX = false;
        }

    }
    private void MoveVertical()
    {

        float vertical = Input.GetAxis("Vertical");
        //playerposition.z += vertical * movespeed;
        rigid.AddForce(new Vector3(0,0,(vertical * movespeed)));

       
    }
    private void UpdatePlayerPosition()
    {
        playerposition = transform.position;
        MoveHorizontal();
        MoveVertical();

        
    }
    public void RemoveCollisionForce()
    {
        rigid.velocity = Vector3.zero;
        print("Remove Force");
    }


    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag.Equals("Score"))
        {
            
            collision.gameObject.SendMessage("AddScore");
        }
        if (collision.gameObject.tag.Equals("Ghost"))
        {
     
            GameManager.Instance.GameOver();
        }
    }

}
