using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemScoreScript : MonoBehaviour
{
    [SerializeField] int score;


    public void AddScore()
    {
        print("Add Score");
        if(GameManager.Instance)
        GameManager.Instance.AddScore(score);

        Destroy(this.gameObject);
    }

    public void SetLifeTime(float lifetime)
    {
       
        Invoke("DestoryItem", lifetime);
    }
    public void DestoryItem()
    {
        Destroy(this.gameObject);
    }
}
