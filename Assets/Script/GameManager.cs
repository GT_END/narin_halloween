using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : Singleton<GameManager>
{
    public float playTime;
    public int Score;
    public GameSetting setting;
    [SerializeField] GameObject itemTemp;
    [SerializeField] GameObject ghostTemp;
    
    [Space]
    public Transform maxXPos;
    public Transform minXPos;
    public Transform maxZPos;
    public Transform minZPos;

    public Transform ghostpos1;
    public Transform ghostpos2;

    public bool isGameover;



    // Start is called before the first frame update
   protected override void Start()
    {
        
        UIManager.Instance.SetScoreText($"Score : {Score}");
        UIManager.Instance.SetScoreResault($"Score : {Score}");
        isGameover = true;
    }
    public void StartGame()
    {
        EventGenerate();
        isGameover = false;
        UIManager.Instance.SetStartGameActive(false);
    }
    // Update is called once per frame
     void Update()
    {
        UIManager.Instance.UpdateTimeUI();


    }

    public void AddScore(int score)
    {
        Score += score;
        UIManager.Instance.UpdateScore(Score);
    }

    public void EventGenerate()
    {
        var spawntime = Random.Range(setting.ItemSettings.minSpawnTime, setting.ItemSettings.maxSpawnTime);

        Invoke("GenerateItem", spawntime);

        var spawntime2 = Random.Range(setting.GhostSettings.minSpawnTime, setting.GhostSettings.maxSpawnTime);

        Invoke("GenerateGhost", spawntime2);
    }

    public Vector3 GetGhostPosition()
    {
        int index = Random.Range(0, 1);


        if(index ==0)
        {
            return ghostpos1.position;
        }
        else
        {
            return ghostpos2.position;
        }

    }

    public Vector3 GetRandomPosition()
    {
        var xpos = Random.Range(minXPos.position.x, maxXPos.position.x);
        var zpos = Random.Range(minZPos.position.z, maxZPos.position.z);


        Vector3 rndpos = new Vector3(xpos, maxXPos.position.y, zpos);

        return rndpos;
    }

    public void GenerateGhost()
    {
        var pos = GetGhostPosition();

        GameObject temp = Instantiate(ghostTemp);

        temp.GetComponent<GhostNavUnit>().SetLifeTime(setting.GhostSettings.LifeTime);

        temp.transform.position = pos;

        temp.SetActive(true);
        var spawntime = Random.Range(setting.GhostSettings.minSpawnTime, setting.GhostSettings.maxSpawnTime);

        Invoke("GenerateGhost", spawntime);

    }
    
    public void GenerateItem()
    {


        var rndpos = GetRandomPosition();

        GameObject temp = Instantiate(itemTemp);

        temp.GetComponent<ItemScoreScript>().SetLifeTime(setting.ItemSettings.itemLifeTime);

        temp.transform.position = rndpos;

        var spawntime = Random.Range(setting.ItemSettings.minSpawnTime, setting.ItemSettings.maxSpawnTime);

        Invoke("GenerateItem", spawntime);

    }
    public void GameOver()
    {
        CancelInvoke();
        print("Game Over");
        isGameover = true;
        UIManager.Instance.SetGameoverActive(true);
    }
    public void RestartGame()
    {
        isGameover = false;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    
    }
}
